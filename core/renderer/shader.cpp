#include "shader.hpp"
#include "math/matrix4.hpp"
#include "math/vector2.hpp"

#include "glad/glad.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <filesystem>

Shader::Shader(unsigned int newId): id{newId} {}

Shader::Shader(const char* filepath)
{
    *this = ParseShader(filepath);
}

Shader Shader::ParseShader(const char* filepath)
{
    std::ifstream stream(filepath);

    enum class ShaderType
    {
        NONE =-1,
        VERTEX = 0,
        FRAGMENT = 1
    };
    
    std::string line;
    std::stringstream ss[2];
    ShaderType type = ShaderType::NONE;

    while (getline(stream, line))
    {
        if(line.find("#shader") != std::string::npos)
        {
            if(line.find("vertex") != std::string::npos)
                type = ShaderType::VERTEX;
            
            else if (line.find("fragment") != std::string::npos)
                type = ShaderType::FRAGMENT;
        }
        else
            ss[(int)type] << line << "\n";      
    }

    std::string vertexShaderSourceStr = ss[0].str(); 
    std::string fragmentShaderSourceStr = ss[1].str();

    const char* vertexShaderSource = vertexShaderSourceStr.c_str();
    const char* fragmentShaderSource = fragmentShaderSourceStr.c_str();


    return CreateShader(vertexShaderSource, fragmentShaderSource);
}

Shader Shader::CreateShader(const char* vertexShaderSource, const char* fragmentShaderSource)
{
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    CheckCompileErrors(vertexShader, "VERTEX");

    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    CheckCompileErrors(fragmentShader, "FRAGMENT");


    //std::cout << "Vertex:\n" << vertexShaderSource << std::endl;
    //std::cout << "Fragment:\n" << fragmentShaderSource << std::endl;

    unsigned int shaderProgram = glCreateProgram();

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);
    CheckCompileErrors(shaderProgram, "PROGRAM");

    // glDetachShader(shaderProgram, vertexShader);
    // glDetachShader(shaderProgram, fragmentShader);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);



    return Shader(shaderProgram);
}

void Shader::SetMatrix(const Matrix4& mat, const char* name)
{
    glUniformMatrix4fv(glGetUniformLocation(id, name), 1, GL_FALSE, mat.e);
}

void Shader::SetInteger(int value, const char* name)
{
    glUniform1i(glGetUniformLocation(id, name), value);
}
void Shader::SetVector4(const Vector4f& vec4, const char* name)
{
    glUniform4fv(glGetUniformLocation(id, name), 1, vec4.e);
}

void Shader::SetVector2(const Vector2f& vec2, const char* name)
{
    glUniform2fv(glGetUniformLocation(id, name), 1, vec2.e);
}
void Shader::Bind() const
{
    glUseProgram(id);
}

void Shader::Unbind() const
{
    glUseProgram(0);
}

void Shader::Delete()
{
    glDeleteProgram(id);
}

void Shader::CheckCompileErrors(unsigned int shader, const char* type)
    {
        int success;
        char infoLog[1024];
        if (type != "PROGRAM")
        {
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                glGetShaderInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
            }
        }
        else
        {
            glGetProgramiv(shader, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
            }
        }
    }