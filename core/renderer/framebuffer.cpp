#include "framebuffer.hpp"
#include "math/vector4.hpp"

#include "glad/glad.h"
#include <iostream>

Framebuffer::Framebuffer(unsigned int _width, unsigned int _height): width{_width}, height{_height}
{
    Init();
}

void Framebuffer::Bind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void Framebuffer::Unbind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::BindTexture() const
{
    glBindTextureUnit(0, textureId);
}

void Framebuffer::UnbindTexture() const
{
    glBindTextureUnit(0, 0);
}

void Framebuffer::ClearColor(const Vector4f& color) const
{
    glClearNamedFramebufferfv(id, GL_COLOR, 0, color.e);
}

void Framebuffer::Init()
{
    glCreateFramebuffers(1, &id);

    glCreateTextures(GL_TEXTURE_2D, 1, &textureId);

    glTextureParameteri(textureId, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTextureParameteri(textureId, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTextureParameteri(textureId, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTextureParameteri(textureId, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
    glNamedFramebufferTexture(id, GL_COLOR_ATTACHMENT0, textureId, 0);
    
    glCreateRenderbuffers(1, &renderbufferId);
    glNamedRenderbufferStorage(renderbufferId, GL_DEPTH24_STENCIL8, width, height);
    glNamedFramebufferRenderbuffer(id, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbufferId);

    auto fboStatus = glCheckNamedFramebufferStatus(id, GL_FRAMEBUFFER);
	if (fboStatus != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer error: " << fboStatus << "\n";
}
void Framebuffer::Delete()
{
    glDeleteFramebuffers(1, &id);
    glDeleteTextures(1, &textureId);
    glDeleteRenderbuffers(1, &renderbufferId);
}


void Framebuffer::Resize(unsigned int newWidth, unsigned int newHeight)
{
    width = newWidth;
    height = newHeight;

    Delete();
    Init();
}