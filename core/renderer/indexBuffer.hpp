#pragma once


class IndexBuffer
{
private:
    unsigned int id;
    unsigned int count;

public:
    IndexBuffer() = default;
    IndexBuffer(const unsigned int* data, unsigned int size);
    ~IndexBuffer();

    //void Bind() const;
    //void Unbind() const;
    void Delete();

    inline unsigned int GetId() const {return id;};
    inline unsigned int GetCount() const {return count;};
};