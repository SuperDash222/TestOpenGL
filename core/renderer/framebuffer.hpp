#pragma once

template<typename T>
struct Vector2;
using Vector2u = Vector2<unsigned int>;

template<typename T>
struct Vector4;
using Vector4f = Vector4<float>;


class Framebuffer
{
private:
    unsigned int id;
    unsigned int textureId;
    unsigned int renderbufferId;

    unsigned int width {200};
    unsigned int height {200};

    void Init();

public:
    Framebuffer(unsigned int _width, unsigned int _height);
    ~Framebuffer() = default;

    void Bind() const;
    void Unbind() const;

    void BindTexture() const;
    void UnbindTexture() const;

    void ClearColor(const Vector4f& color) const;

    void Delete();
    void Resize(unsigned int newWidth, unsigned int newHeight);

    inline int GetId(){return id;}
    inline int GetTextureId(){return textureId;}

    inline unsigned int GetWidth(){return width;}
    inline unsigned int GetHeight(){return height;}

};