#pragma once
#include "glad/glad.h"

#include <vector>

struct VertexBufferElement
{
    unsigned int type;
    unsigned int count;
    unsigned char normalized;

    static unsigned int GetSizeOfType(unsigned int type)
    {
        switch (type)
        {
        case GL_FLOAT: return 4;
        case GL_UNSIGNED_INT: return 4;
        case GL_UNSIGNED_BYTE: return 1;
        }

        return 0;
    }
};

class VertexBufferLayout
{
private:
    std::vector<VertexBufferElement> elements;
    unsigned int stride{0};

public:
    VertexBufferLayout() = default;
    
    template<typename T>
    inline void Push(unsigned int count);

    inline unsigned int GetStride() const {return stride;}
    inline const std::vector<VertexBufferElement>& GetElements() const {return elements;}
};

#include "vertexBufferLayout.inl"