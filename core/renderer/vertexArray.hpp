#pragma once

class VertexBuffer;
class IndexBuffer;
class VertexBufferLayout;

class VertexArray
{
private:
    unsigned int id;
public:
    VertexArray();

    void AddBuffer(const VertexBuffer& vertexBuffer, const IndexBuffer& indexBuffer, const VertexBufferLayout& vertexBufferLayout);
    void Bind() const;
    void Delete();

    inline unsigned int GetId() const {return id;};
};