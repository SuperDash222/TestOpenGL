#include "indexBuffer.hpp"
#include "glad/glad.h"

IndexBuffer::IndexBuffer(const unsigned int* data, unsigned int count): count{count}
{
    glCreateBuffers(1, &id);
    glNamedBufferData(id, count, data, GL_STATIC_DRAW);
    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW);
}

IndexBuffer::~IndexBuffer()
{
    
}

//void IndexBuffer::Bind() const
//{
//    glBindBuffer(GL_ARRAY_BUFFER, id);
//}
//
//void IndexBuffer::Unbind() const
//{
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//}

void IndexBuffer::Delete()
{
    glDeleteBuffers(1, &id);
}