#pragma once

template<typename T>
struct Vector4;
using Vector4f = Vector4<float>;
class VertexArray;
class IndexBuffer;
class Shader;

struct Renderer
{
public:
    static void Init();
    static void Draw(const VertexArray& vertexArray, const IndexBuffer& indexBuffer, const Shader& shader);
    static void Clear();
    static void SetClearColor(const Vector4f& color);
};