#pragma once

#include <string>

class Texture
{
private:
    unsigned int id;
    std::string filepath;
    unsigned char* buffer;
    int width, height, BPP;

public:
    Texture() = default;
    Texture(const std::string& path);

    void Bind(unsigned int slot = 0) const;
    void Unbind();
    void Delete();

    inline unsigned int GetWidth() const {return width;};
    inline unsigned int GetHeight() const {return height;};
    inline unsigned int GetId() const {return id;};
};