#include "texture.hpp"
#include "glad/glad.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

Texture::Texture(const std::string& path):
id{0}, filepath{path}, buffer{nullptr}, width{0}, height{0}, BPP{0}
{
    stbi_set_flip_vertically_on_load(1);
    buffer = stbi_load(path.c_str(), &width, &height, &BPP, 4);

    glCreateTextures(GL_TEXTURE_2D, 1, &id);

    glTextureParameteri(id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTextureParameteri(id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glTextureParameteri(id, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glDisable(GL_BLEND);

    glTextureStorage2D(id, 1, GL_RGBA8, width, height);
    glTextureSubImage2D(id, 0, 0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

    if(buffer)
        stbi_image_free(buffer);
}

void Texture::Bind(unsigned int slot) const
{
    glBindTextureUnit(slot, id);
}
void Texture::Unbind()
{
    glBindTextureUnit(0, 0);
}

void Texture::Delete()
{
    glDeleteTextures(1, &id);
}