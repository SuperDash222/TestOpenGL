#pragma once


class VertexBuffer
{
private:
    unsigned int id;

public:
    VertexBuffer() = default;
    VertexBuffer(const void* data, unsigned int size);

    //void Bind() const;
    //void Unbind() const;
    void Delete();

    inline unsigned int GetId() const {return id;};

};