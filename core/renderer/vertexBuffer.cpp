#include "vertexBuffer.hpp"
#include "glad/glad.h"

VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
    glCreateBuffers(1, &id);
    glNamedBufferData(id, size, data, GL_STATIC_DRAW);
}

//void VertexBuffer::Bind() const
//{
//    glBindBuffer(GL_ARRAY_BUFFER, id);
//}
//
//void VertexBuffer::Unbind() const
//{
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//}

void VertexBuffer::Delete()
{
    glDeleteBuffers(1, &id);
}