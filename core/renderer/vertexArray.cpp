#include "vertexArray.hpp"
#include "vertexBuffer.hpp"
#include "indexBuffer.hpp"
#include "vertexBufferLayout.hpp"

VertexArray::VertexArray()
{
    glCreateVertexArrays(1, &id);
}

void VertexArray::AddBuffer(const VertexBuffer& vertexBuffer, const IndexBuffer& indexBuffer, const VertexBufferLayout& vertexBufferLayout)
{
    Bind();

    const auto& elements = vertexBufferLayout.GetElements();
    unsigned int offset = 0;

    for(int i = 0; i < elements.size(); i++)
    {
        const VertexBufferElement& element = elements[i];

        glEnableVertexArrayAttrib(id, i);
	    glVertexArrayAttribBinding(id, i, 0);
	    glVertexArrayAttribFormat(id, i, element.count, element.type, element.normalized, offset);

        //glEnableVertexAttribArray(i);
        //glVertexAttribPointer(i, element.count, element.type, element.normalized, vertexBufferLayout.GetStride(), (const void*)offset);
        offset += element.count * VertexBufferElement::GetSizeOfType(element.type);
    }

    glVertexArrayVertexBuffer(id, 0, vertexBuffer.GetId(), 0, vertexBufferLayout.GetStride());
	glVertexArrayElementBuffer(id, indexBuffer.GetId());
}

void VertexArray::Bind() const
{
    glBindVertexArray(id);
}

void VertexArray::Delete()
{
    glDeleteVertexArrays(1, &id);
}