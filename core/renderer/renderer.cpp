#include "renderer.hpp"
#include "math/vector4.hpp"
#include "vertexArray.hpp"
#include "indexBuffer.hpp"
#include "shader.hpp"

#include "glad/glad.h"

void Renderer::Init()
{
    glEnable(GL_DEPTH_TEST);
}

void Renderer::Draw(const VertexArray& vertexArray,const IndexBuffer& indexBuffer, const Shader& shader)
{
    shader.Bind();
    vertexArray.Bind();
    glDrawElements(GL_TRIANGLES, indexBuffer.GetCount(), GL_UNSIGNED_INT, 0);
}

void Renderer::Clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::SetClearColor(const Vector4f& color)
{
    glClearColor(color.x, color.y, color.z, color.w);
}