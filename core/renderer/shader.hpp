#pragma once

struct Matrix4;
template<typename T>
struct Vector4;
using Vector4f = Vector4<float>;

template<typename T>
struct Vector2;
using Vector2f = Vector2<float>;

class Shader
{
private:
    unsigned int  id{0};
    Shader(unsigned int newId);

    static void CheckCompileErrors(unsigned int shader, const char* type);
public:
    Shader() = default;
    Shader(const char* filepath);
    ~Shader() = default;

    static Shader ParseShader(const char* filepath);
    static Shader CreateShader(const char* vertexShaderSource, const char* fragmentShaderSource);

    void SetMatrix(const Matrix4& mat, const char* name);
    void SetInteger(int value, const char* name);
    void SetVector4(const Vector4f& vec4, const char* name);
    void SetVector2(const Vector2f& vec2, const char* name);

    void Bind() const;
    void Unbind() const;
    void Delete();
};