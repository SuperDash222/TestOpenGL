#pragma once

#include "math/vector2.hpp"
#include "math/vector3.hpp"

struct Vertex
{
    Vector3f pos{0, 0, 0};
    Vector2f uv{0, 0};
};