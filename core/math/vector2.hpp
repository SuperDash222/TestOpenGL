#pragma once

template <typename T>
struct Vector2
{
    union
    {
        struct
        {
            T x, y;
        };
        T e[2]{0};
    };


    void Normalize();
    Vector2<T> GetNormalize() const;
    
    T Length() const;
    T SqLength() const;

    static T DotProduct(const Vector2<T> &v1, const Vector2<T> &v2);

    Vector2<T> operator+(const Vector2<T> &vec) const;
    Vector2<T> operator-(const Vector2<T> &vec) const;
    Vector2<T> operator*(T value) const;
};

using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;

#include "vector2.inl"