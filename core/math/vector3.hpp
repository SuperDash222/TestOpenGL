#pragma once

template <typename T>
struct Vector3
{
    union
    {
        struct
        {
            T x, y, z;
        };
        T e[3]{0};
    };


    void Normalize();
    Vector3<T> GetNormalize() const;
    
    T Length() const;
    T SqLength() const;

    static T DotProduct(const Vector3<T> &v1, const Vector3<T> &v2);
    static Vector3<T> CrossProduct(const Vector3<T> &v1, const Vector3<T> &v2);

    Vector3<T> operator+(const Vector3<T> &vec) const;
    Vector3<T> operator-(const Vector3<T> &vec) const;
    Vector3<T> operator*(T value) const;
};

using Vector3f = Vector3<float>;
using Vector3d = Vector3<double>;

#include "vector3.inl"