#include <cmath>

template<typename T>
T Vector4<T>::Length() const
{
    return sqrt(x * x + y * y + z * z + w * w);
}
template<typename T>
T Vector4<T>::SqLength() const
{
    return x * x + y * y + z * z + w * w;
}

template<typename T>
void Vector4<T>::Homogenize()
{
    if (w != 0 && w != 1)
    {
        x /= w;
        y /= w;
        z /= w;
    }
}

template<typename T>
Vector4<T> Vector4<T>::GetHomogenize()
{
    if (w != 0 && w != 1)
        return {x/w, y/w, z/w, w};
    else
        return this;
}

template<typename T>
T Vector4<T>::DotProduct(const Vector4<T> &v1, const Vector4<T> &v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
}

template<typename T>
Vector4<T> Vector4<T>::operator+(const Vector4<T> &vec) const
{
    return {x + vec.x, y + vec.y, z + vec.z, w + vec.w};
}

template<typename T>
Vector4<T> Vector4<T>::operator-(const Vector4<T> &vec) const
{
    return {x - vec.x, y - vec.y, z - vec.z, w - vec.w};
}

template<typename T>
Vector4<T> Vector4<T>::operator*(T value) const
{
    return {x * value, y * value, z * value, w * value};
}