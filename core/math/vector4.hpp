#pragma once

template <typename T>
struct Vector4
{
    union
    {
        struct
        {
            T x, y, z, w;
        };
        T e[4]{0};
    };

    T Length() const;
    T SqLength() const;

    void Homogenize();
    Vector4<T> GetHomogenize();
    
    static T DotProduct(const Vector4<T> &v1, const Vector4<T> &v2);

    Vector4<T> operator+(const Vector4<T> &vec) const;
    Vector4<T> operator-(const Vector4<T> &vec) const;
    Vector4<T> operator*(T value) const;
};

using Vector4f = Vector4<float>;
using Vector4d = Vector4<double>;

#include "vector4.inl"