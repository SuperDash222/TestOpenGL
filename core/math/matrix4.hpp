#pragma once
#include <cmath>

#include "vector3.hpp"
#include "vector4.hpp"

struct Matrix4
{
	union
	{
		Vector4f v[4];
		float e[16]{ 0 };
	};

	inline static Matrix4 Identity();
	inline static Matrix4 Scale(float scale);
	inline static Matrix4 Scale(const Vector3f& scale);
	inline static Matrix4 Translate(const Vector3f& translate);
	inline static Matrix4 XAxisRotation(float angle);
	inline static Matrix4 YAxisRotation(float angle);
	inline static Matrix4 ZAxisRotation(float angle);
	inline static Matrix4 Rotation(const Vector3f& rotation);
	inline static Matrix4 Perspective(float yFov, float aspect, float n, float f);
	inline static Matrix4 Orthographic(float left, float right, float bottom, float top, float near, float far);

	inline Matrix4 operator*(const Matrix4& matrix) const;
	inline Vector4f operator*(const Vector4f& vector4) const;
};

#include "matrix4.inl"