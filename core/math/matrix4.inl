Matrix4 Matrix4::Identity()
{
	return
	{
		1 ,0, 0, 0,
		0 ,1, 0, 0,
		0 ,0, 1, 0,
		0 ,0, 0, 1
	};
}

Matrix4 Matrix4::Scale(float scale)
{
	return
	{
		scale, 0, 0, 0,
		0, scale, 0, 0,
		0, 0, scale, 0,
		0, 0, 0, 1
	};
}
Matrix4 Matrix4::Scale(const Vector3f& scale)
{
	return
	{
		scale.x, 0, 0, 0,
		0, scale.y, 0, 0,
		0, 0, scale.z, 0,
		0, 0, 0, 1
	};
}

Matrix4 Matrix4::Translate(const Vector3f& translate)
{
	return
	{
		1 ,0, 0, 0,
		0 ,1, 0, 0,
		0 ,0, 1, 0,
		translate.x , translate.y, translate.z, 1
	};
}

Matrix4 Matrix4::XAxisRotation(float angle)
{
	return
	{
		1, 0, 0, 0,
		0, cosf(angle), -sinf(angle), 0,
		0, sinf(angle), cosf(angle), 0,
		0, 0, 0, 1
	};
}

Matrix4 Matrix4::YAxisRotation(float angle)
{
	return
	{
		cosf(angle), 0, sinf(angle), 0,
		0, 1, 0, 0,
		-sinf(angle), 0, cosf(angle), 0,
		0, 0, 0, 1
	};
}

Matrix4 Matrix4::ZAxisRotation(float angle)
{
	return
	{
		cosf(angle), -sinf(angle), 0, 0,
		sinf(angle), cosf(angle), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
}

Matrix4 Matrix4::Rotation(const Vector3f& rotation)
{
	return YAxisRotation(rotation.y) * XAxisRotation(rotation.x) * ZAxisRotation(rotation.z);
}

Matrix4 Matrix4::Perspective(float fov, float aspect, float near, float far)
{
	Matrix4 result{0};
	float const a = 1.f / tanf(fov / 2.f);

	result.v[0].e[0] = a / aspect;
	result.v[1].e[1] = a; 
	result.v[2].e[2] = -((far + near) / (far - near));
	result.v[2].e[3] = -1.f;
	result.v[3].e[2] = -((2.f * far * near) / (far - near));

	return result;
}

Matrix4 Matrix4::Orthographic(float left, float right, float bottom, float top, float near, float far)
{
	Matrix4 result{0};

	float rl = right - left;
    float tb = top - bottom;
    float fn = far - near;

	result.v[0].e[0] = 2.0f / rl;
	result.v[1].e[1] = 2.0f / tb;
	result.v[2].e[2] = -2.0f / fn;
	result.v[3].e[0] = -(left + right) / rl;
	result.v[3].e[1] = -(top + bottom) / tb;
	result.v[3].e[2] = -(far + near) / fn;
	result.v[3].e[3] = 1.0f;

	return result;
}

Matrix4 Matrix4::operator*(const Matrix4& matrix) const
{
	Matrix4 result{0};
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			for (int k = 0; k < 4; k++)
				result.v[i].e[j] += v[k].e[j] * matrix.v[i].e[k];

	return result;
}

Vector4f Matrix4::operator*(const Vector4f& vector) const
{
	Vector4f result;

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.e[i] += vector.e[j] * v[i].e[j];

	return result;
}