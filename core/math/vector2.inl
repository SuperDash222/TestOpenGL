#include <cmath>

template<typename T>
void Vector2<T>::Normalize()
{
    T length = Length();
    x /= length;
    y /= length;
}

template<typename T>
Vector2<T> Vector2<T>::GetNormalize() const
{
    T length = Length();
    return {x/length, y/length};
}

template<typename T>
T Vector2<T>::Length() const
{
    return sqrt(x * x + y * y);
}

template<typename T>
T Vector2<T>::SqLength() const
{
    return x * x + y * y;
}

template<typename T>
T Vector2<T>::DotProduct(const Vector2<T> &v1, const Vector2<T> &v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}

template<typename T>
Vector2<T> Vector2<T>::operator+(const Vector2<T> &vec) const
{
    return {x + vec.x, y + vec.y};
}

template<typename T>
Vector2<T> Vector2<T>::operator-(const Vector2<T> &vec) const
{
    return {x - vec.x, y - vec.y};
}

template<typename T>
Vector2<T> Vector2<T>::operator*(T value) const
{
    return {x * value, y * value};
}