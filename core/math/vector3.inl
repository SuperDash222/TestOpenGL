#include <cmath>

template<typename T>
void Vector3<T>::Normalize()
{
    T length = Length();
    x /= length;
    y /= length;
    z /= length;
}

template<typename T>
Vector3<T> Vector3<T>::GetNormalize() const
{
    T length = Length();
    return {x/length, y/length, z/length};
}

template<typename T>
T Vector3<T>::Length() const
{
    return sqrt(x * x + y * y + z * z);
}

template<typename T>
T Vector3<T>::SqLength() const
{
    return x * x + y * y + z * z;
}

template<typename T>
T Vector3<T>::DotProduct(const Vector3<T> &v1, const Vector3<T> &v2)
{
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

template<typename T>
Vector3<T> Vector3<T>::CrossProduct(const Vector3<T> &v1, const Vector3<T> &v2)
{
    return {
        v1.y * v2.z - v2.y * v1.z,
        v1.z * v2.x - v2.z * v1.x,
        v1.x * v2.y - v2.x * v1.y
        };
}

template<typename T>
Vector3<T> Vector3<T>::operator+(const Vector3<T> &vec) const
{
    return {x + vec.x, y + vec.y, z + vec.z};
}

template<typename T>
Vector3<T> Vector3<T>::operator-(const Vector3<T> &vec) const
{
    return {x - vec.x, y - vec.y, z - vec.z};
}

template<typename T>
Vector3<T> Vector3<T>::operator*(T value) const
{
    return {x * value, y * value, z * value};
}