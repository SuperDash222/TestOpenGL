#shader vertex
#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texCoord;

uniform mat4 trs;
uniform mat4 perspective;
uniform mat4 view;
uniform vec2 cursor;

out vec2 v_TexCoord;

void main()
{
   gl_Position = perspective * view * trs * vec4(aPos, 1.0f);
   float step = 1.0f/16.0f;
   v_TexCoord = texCoord * step + cursor * step;
};

#shader fragment
#version 460 core

out vec4 FragColor;
in vec2 v_TexCoord;

uniform sampler2D u_Texture;

void main()
{
   FragColor = texture(u_Texture, v_TexCoord);
};