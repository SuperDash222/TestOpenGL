#shader vertex
#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texCoord;

uniform mat4 trs;
uniform mat4 perspective;
uniform vec4 color;
out vec4 v_Color;

void main()
{
   gl_Position = perspective * trs * vec4(aPos, 1.0f);
   v_Color = color;
};

#shader fragment
#version 460 core

out vec4 FragColor;
in vec4 v_Color;

uniform sampler2D u_Texture;

void main()
{
   FragColor = v_Color;
};