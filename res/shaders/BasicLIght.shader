#shader vertex
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texCoord;

uniform mat4 trs;
uniform mat4 perspective;
uniform mat4 view;
uniform vec4 lightColor;

out vec2 v_TexCoord;
out vec4 v_Color;

void main()
{
   gl_Position = perspective * view * trs * vec4(aPos, 1.0f);
   v_TexCoord = texCoord;
   v_Color = lightColor;
};

#shader fragment
#version 330 core

out vec4 FragColor;
in vec2 v_TexCoord;
in vec4 v_Color;

uniform sampler2D u_Texture;

void main()
{
   vec4 texColor = texture(u_Texture, v_TexCoord);
   FragColor = v_Color * texColor;
};