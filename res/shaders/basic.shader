#shader vertex
#version 460 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 texCoord;

uniform mat4 trs;
uniform mat4 perspective;
uniform mat4 view;

out vec2 v_TexCoord;

void main()
{
   gl_Position = perspective * view * trs * vec4(aPos, 1.0f);
   v_TexCoord = texCoord;
};

#shader fragment
#version 460 core

out vec4 FragColor;
in vec2 v_TexCoord;

uniform sampler2D u_Texture;

void main()
{
   vec4 texColor = texture(u_Texture, v_TexCoord);
   FragColor = texColor;
   //FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
};