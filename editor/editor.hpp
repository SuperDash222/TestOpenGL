#pragma once

#include "renderer/framebuffer.hpp"

struct GLFWwindow;

class Editor
{
private:
    GLFWwindow* window;
    
    void InspectorWindow();
    void MenuWindow();
    void SceneWindow();
    void DockingWindow();
public:
    Editor(GLFWwindow* _window);
    ~Editor() = default;

    Framebuffer sceneFramebuffer;
    unsigned int applicationIndex{0};
    unsigned int nbApplication{0};

    void Update();
};