#include "textureAtlasApplication.hpp"

#include "renderer/renderer.hpp"
#include "renderer/vertex.hpp"

#include "GLFW/glfw3.h"
#include <iostream>

TextureAtlasApplication::TextureAtlasApplication(GLFWwindow* _window): Application(_window), trs{Matrix4::Identity()}, perspective{Matrix4::Identity()}
{
    #if linux
    shader = Shader("res/shaders/atlas.shader");
    texture = Texture("res/textures/SoartexFanver.png");
    #elif _WIN32
    shader = Shader("../../res/shaders/atlas.shader");
    texture = Texture("../../res/textures/SoartexFanver.png");
    #endif

    const Vertex vertices[] 
    {
        //Front
        {{1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},//0
        {{1.0f, -1.0f, 1.0f}, {1.0f, 0.0f}},//1
        {{-1.0f, -1.0f, 1.0f}, {0.0f, 0.0f}},//2
        {{-1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},//3

        //Back
        {{1.0f, 1.0f, -1.0f}, {1.0f, 1.0f}},//4
        {{1.0f, -1.0f, -1.0f}, {1.0f, 0.0f}},//5
        {{-1.0f, -1.0f, -1.0f}, {0.0f, 0.0f}},//6
        {{-1.0f, 1.0f, -1.0f}, {0.0f, 1.0f}},//7

        //Left
        {{-1.0f, 1.0f, -1.0f}, {1.0f, 1.0f}},//8
        {{-1.0f, -1.0f, -1.0f}, {1.0f, 0.0f}},//9
        {{-1.0f, -1.0f, 1.0f}, {0.0f, 0.0f}},//10
        {{-1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},//11

        //Right
        {{1.0f, 1.0f, -1.0f}, {1.0f, 1.0f}},//12
        {{1.0f, -1.0f, -1.0f}, {1.0f, 0.0f}},//13
        {{1.0f, -1.0f, 1.0f}, {0.0f, 0.0f}},//14
        {{1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},//15

        //Top
        {{1.0f, 1.0f, 1.0f}, {1.0f, 1.0f}},//16
        {{1.0f, 1.0f, -1.0f}, {1.0f, 0.0f}},//17
        {{-1.0f, 1.0f, -1.0f}, {0.0f, 0.0f}},//18
        {{-1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}},//19

        //Down
        {{1.0f, -1.0f, 1.0f}, {1.0f, 1.0f}},//20
        {{1.0f, -1.0f, -1.0f}, {1.0f, 0.0f}},//21
        {{-1.0f, -1.0f, -1.0f}, {0.0f, 0.0f}},//22
        {{-1.0f, -1.0f, 1.0f}, {0.0f, 1.0f}}//23
    };

    const unsigned int indices[]
    {
        //Front
        0, 1, 3,
        1, 2, 3,
        //Back
        4, 5, 7,
        5, 6, 7,
        //Left
        8, 9, 11,
        9, 10, 11,
        //Right
        12, 13, 15,
        13, 14, 15,
        //Top
        16, 17, 19,
        17, 18, 19,
        //Down
        20, 21, 23,
        21, 22, 23
    };


    Renderer::Init(); 
    
    vertexBuffer = VertexBuffer(vertices, sizeof(vertices));
    indexBuffer = IndexBuffer(indices, sizeof(indices));

    layout.Push<float>(3);
    layout.Push<float>(2);
    
    vertexArray.AddBuffer(vertexBuffer, indexBuffer, layout);

    glEnableVertexAttribArray(0);
    glEnable(GL_DEPTH_TEST);

    float aspect = (float)width / (float)height;

    perspective = Matrix4::Perspective(3.14f/2.f, aspect, 0.01f, 100.0f);
    trs = Matrix4::Translate({0, 0, 0});

    shader.Bind();
    shader.SetMatrix(perspective, "perspective");

    texture.Bind();
    shader.SetInteger(0, "u_Texture");
}


TextureAtlasApplication::~TextureAtlasApplication() {}

void TextureAtlasApplication::Start() {}

void TextureAtlasApplication::Update()
{
    Renderer::SetClearColor({0.2f, 0.2f, 0.2f, 1.0f});
    Renderer::Clear();
    shader.Bind();
    texture.Bind();
    InputProcess();

    trs = trs * Matrix4::Rotation({0.f, angularSpeed * 0.001f, 0.f});
    shader.SetMatrix(trs, "trs");
    shader.SetMatrix(Matrix4::Translate({0.f, 0.f, -5.f}), "view");
    shader.SetVector2(cursor, "cursor");

    Renderer::Draw(vertexArray, indexBuffer, shader);
}

void TextureAtlasApplication::End()
{
    vertexArray.Delete();
    vertexBuffer.Delete();
    indexBuffer.Delete();
    shader.Delete();
    texture.Delete();
}

void TextureAtlasApplication::InputProcess()
{
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS && !pressOnce)
    {
        cursor.x = ((int)cursor.x + 1) % 16;
        std::cout << cursor.x << '\n';

        pressOnce = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS && !pressOnce)
    {
        cursor.x = cursor.x - 1 < 0? 15 : cursor.x - 1;
        std::cout << cursor.x << '\n';
        pressOnce = true;   
    }
    else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS && !pressOnce)
    {
        cursor.y = ((int)cursor.y + 1) % 16;
        std::cout << cursor.y << '\n';

        pressOnce = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS && !pressOnce)
    {
        cursor.y = cursor.y - 1 < 0 ? 15 : cursor.y - 1;
        std::cout << cursor.y << '\n';
        pressOnce = true;
    }
    else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_RELEASE &&
             glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_RELEASE &&
             glfwGetKey(window, GLFW_KEY_UP) == GLFW_RELEASE &&
             glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_RELEASE &&
             pressOnce)
    {
        pressOnce = false;
    }
}