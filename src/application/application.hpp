#pragma once

struct GLFWwindow;

class Application
{
protected:
    int width{800};
    int height{600};

    GLFWwindow* window;
    virtual void InputProcess() = 0;
public:
    Application() = default;
    Application(GLFWwindow* _window): window{_window} {}
    ~Application() = default;

    virtual void Start() = 0;
    virtual void Update() = 0;
    virtual void End() = 0;
};