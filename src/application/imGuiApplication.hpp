#pragma once

#include "application.hpp"
#include "imgui.h"
#include "math/vector4.hpp"

class ImGuiApplication : public Application
{
private:    
    Vector4f clearColor;

    bool showDemo = false;
    bool docked = true;
    ImGuiDockNodeFlags dockspaceFlags {ImGuiDockNodeFlags_None};
    bool showAnotherWindow = false;
    float slider = 0.0f;
    int counter = 0;

    void InputProcess() override;
    
public:
    ImGuiApplication(GLFWwindow* _window);
    ~ImGuiApplication();

    void Start() override;
    void Update() override;
    void End() override;
};