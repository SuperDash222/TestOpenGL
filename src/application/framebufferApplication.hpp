#pragma once

#include "application.hpp"

#include "renderer/shader.hpp"
#include "renderer/vertexBuffer.hpp"
#include "renderer/indexBuffer.hpp"
#include "renderer/vertexArray.hpp"
#include "renderer/vertexBufferLayout.hpp"
#include "renderer/framebuffer.hpp"

#include "math/matrix4.hpp"

#include "textureAtlasApplication.hpp"

class FramebufferApplication : public Application
{
private:
    void InputProcess() override;
    Framebuffer framebuffer;
    TextureAtlasApplication textureAtlasApplication;
    
    Shader shader;

    VertexArray vertexArray;
    VertexBufferLayout layout;
    VertexBuffer vertexBuffer;
    IndexBuffer indexBuffer;

    Matrix4 trs;

public:
    FramebufferApplication(GLFWwindow* _window);
    ~FramebufferApplication();

    void Start() override;
    void Update() override;
    void End() override;
};