#include "physics2DApplication.hpp"
#include "renderer/renderer.hpp"
#include "renderer/vertex.hpp"
#include "math/vector4.hpp"

Physics2DApplication::Physics2DApplication(GLFWwindow* _window): Application(_window), trs{Matrix4::Translate({400.f, 300.f}) * Matrix4::Scale({200.f})},
                                                                 perspective{Matrix4::Orthographic(0.0f, 800.0f, 0.0f, 600.0f, 0.0f, 10.0f)}
{
    #if linux
    shader = Shader("res/shaders/2D.shader");
    #elif _WIN32
    shader = Shader("../../res/shaders/2D.shader");    
    #endif

    const Vertex vertices[] 
    {
        {{1.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
        {{1.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-1.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},
        {{-1.0f, 1.0f, 0.0f}, {0.0f, 1.0f}}
    };

    const unsigned int indices[]
    {
        0,1,3,
        1,2,3
    };

    vertexBuffer = VertexBuffer(vertices, sizeof(vertices));
    indexBuffer = IndexBuffer(indices, sizeof(indices));

    layout.Push<float>(3);
    layout.Push<float>(2);
    
    vertexArray.AddBuffer(vertexBuffer, indexBuffer, layout);

    shader.Bind();
    shader.SetMatrix(trs, "trs");
    shader.SetMatrix(perspective, "perspective");
    shader.SetVector4({0.2f, 0.6f, 0.8f, 1.0f}, "color");
}

void Physics2DApplication::Start() {}

void Physics2DApplication::Update()
{
    Renderer::SetClearColor({0.2f, 0.2f, 0.2f, 1.0f});
    Renderer::Clear();

    Renderer::Draw(vertexArray, indexBuffer, shader);
}

void Physics2DApplication::End()
{
    shader.Delete();
    vertexArray.Delete();
    indexBuffer.Delete();
    vertexBuffer.Delete();
}

void Physics2DApplication::InputProcess() {}