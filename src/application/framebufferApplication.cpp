#include "framebufferApplication.hpp"

#include "renderer/renderer.hpp"
#include "glad/glad.h"
#include "renderer/vertex.hpp"
#include "math/vector3.hpp"
#include "math/vector4.hpp"

FramebufferApplication::FramebufferApplication(GLFWwindow* _window) : Application(_window), framebuffer{800, 600}, textureAtlasApplication{_window}, trs{Matrix4::Identity()}
{
    #if linux
    shader = Shader("res/shaders/basic.shader");
    #elif _WIN32
    shader = Shader("../../res/shaders/basic.shader");    
    #endif

    const Vertex vertices[] 
    {
        {{1.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
        {{1.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-1.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},
        {{-1.0f, 1.0f, 0.0f}, {0.0f, 1.0f}}
    };

    const unsigned int indices[]
    {
        0,1,3,
        1,2,3
    };

    vertexBuffer = VertexBuffer(vertices, sizeof(vertices));
    indexBuffer = IndexBuffer(indices, sizeof(indices));

    layout.Push<float>(3);
    layout.Push<float>(2);
    
    vertexArray.AddBuffer(vertexBuffer, indexBuffer, layout);

    glEnableVertexAttribArray(0);
    glEnable(GL_DEPTH_TEST);

    shader.Bind();
    shader.SetMatrix(Matrix4::Scale({ 0.5f }), "trs");
    shader.SetMatrix(Matrix4::Identity(), "perspective");
    shader.SetMatrix(Matrix4::Identity(), "view");
    shader.SetInteger(0, "u_Texture");
}

FramebufferApplication::~FramebufferApplication(){}

void FramebufferApplication::Start(){}

void FramebufferApplication::Update()
{
    Renderer::SetClearColor({0.6f, 0.2f, 0.2f, 1.0f});
    Renderer::Clear();

    framebuffer.Bind();
    glViewport(0, 0, framebuffer.GetWidth(), framebuffer.GetHeight());
    textureAtlasApplication.Update();
    framebuffer.Unbind();
    shader.Bind();
    framebuffer.BindTexture();
    Renderer::Draw(vertexArray, indexBuffer, shader);
}

void FramebufferApplication::End()
{
    textureAtlasApplication.End();
    framebuffer.Delete();
    vertexArray.Delete();
    vertexBuffer.Delete();
    indexBuffer.Delete();
    shader.Delete();
}

void FramebufferApplication::InputProcess(){}