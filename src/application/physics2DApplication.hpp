#pragma once

#include "application.hpp"
#include "renderer/shader.hpp"
#include "renderer/vertexBuffer.hpp"
#include "renderer/indexBuffer.hpp"
#include "renderer/vertexArray.hpp"
#include "renderer/vertexBufferLayout.hpp"
#include "math/matrix4.hpp"

class Physics2DApplication : public Application
{
private:
    Shader shader;

    float speed{5.f};
    float angularSpeed{5.f};
    VertexArray vertexArray;
    VertexBufferLayout layout;
    VertexBuffer vertexBuffer;
    IndexBuffer indexBuffer;

    Matrix4 perspective;
    Matrix4 trs;
    void InputProcess() override;

public:
    Physics2DApplication(GLFWwindow* _window);
    ~Physics2DApplication() = default;
    void Start() override;
    void Update() override;
    void End() override;
};