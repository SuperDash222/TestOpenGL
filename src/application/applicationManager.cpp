#include "applicationManager.hpp"
#include "application.hpp"

#include <iostream>


ApplicationManager::~ApplicationManager()
{
    for(Application* app : applications)
    {
        delete(app);
    }
}

void ApplicationManager::Update()
{
    if(applications.size() < 1)
    {
        std::cout << "There is no app in the manager.\n";
        return;
    }
     
    applications[currentApplicationIndex]->Update();
}

void ApplicationManager::EndApplications()
{
    for(Application* app : applications)
    {
        app->End();
    }
}

void ApplicationManager::AddApplication(Application* app)
{
    applications.push_back(app);
}

void ApplicationManager::SetCurrentApplication(unsigned int applicationIndex)
{
    if(applicationIndex < applications.size())
        currentApplicationIndex = applicationIndex;
}