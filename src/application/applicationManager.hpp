#pragma once

#include <vector>

class Application;

class ApplicationManager
{
private:
    std::vector<Application*> applications;
    unsigned int currentApplicationIndex = 0;
public:
    ApplicationManager() = default;
    ~ApplicationManager();
    

    void Update();
    void EndApplications();
    void AddApplication(Application* app);
    void SetCurrentApplication(unsigned int applicationIndex);

    inline unsigned int GetCurrentApplicationIndex(){return currentApplicationIndex;}
    inline unsigned int GetNbApplication(){return applications.size();}
};