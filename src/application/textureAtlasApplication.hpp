#pragma once

#include "application.hpp"

#include "renderer/shader.hpp"
#include "renderer/texture.hpp"
#include "renderer/vertexBuffer.hpp"
#include "renderer/indexBuffer.hpp"
#include "renderer/vertexArray.hpp"
#include "renderer/vertexBufferLayout.hpp"

#include "math/vector2.hpp"
#include "math/vector3.hpp"
#include "math/matrix4.hpp"

class TextureAtlasApplication : public Application
{
private:
    Shader shader;
    Texture texture;

    float speed{5.f};
    float angularSpeed{5.f};
    Vector2f cursor{0,0};
    bool pressOnce{false};

    VertexArray vertexArray;
    VertexBufferLayout layout;
    VertexBuffer vertexBuffer;
    IndexBuffer indexBuffer;

    Matrix4 perspective;
    Matrix4 trs;

    void InputProcess() override;
    
public:
    TextureAtlasApplication(GLFWwindow* window);
    ~TextureAtlasApplication();

    void Start() override;
    void Update() override;
    void End() override;
};