#include <iostream>

#include "glad/glad.h"
#include "GLFW/glfw3.h"

#include "application/applicationManager.hpp"
#include "application/textureAtlasApplication.hpp"
#include "application/framebufferApplication.hpp"
#include "application/physics2DApplication.hpp"

#include "renderer/renderer.hpp"
#include "renderer/rendererManager.hpp"
#include "renderer/framebuffer.hpp"

#include "math/vector2.hpp"

#include "editor/editor.hpp"

int main()
{
    int width = 800;
    int height = 600;

    int prevWidth = width;
    int prevHeight = height;

    glfwInit();
    glfwInitHint(GLFW_VERSION_MAJOR, 4);
    glfwInitHint(GLFW_VERSION_MINOR, 6);
    glfwInitHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);


    GLFWwindow* window = glfwCreateWindow(width, height, "Test", NULL, NULL);
    if(window == NULL)
    {
        std::cout << "failed to create window.\n";
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "failed to init GLAD.\n";
        glfwTerminate();
        return -1;
    }
    glViewport(0, 0, width, height);
    RendererManager rendererManager;

    ApplicationManager appManager;
    appManager.AddApplication(new TextureAtlasApplication(window));
    appManager.AddApplication(new FramebufferApplication(window));
    appManager.AddApplication(new Physics2DApplication(window));

    Editor editor(window);
    editor.applicationIndex = appManager.GetCurrentApplicationIndex();
    editor.nbApplication = appManager.GetNbApplication();

    Framebuffer* framebuffer = &editor.sceneFramebuffer;

    while (!glfwWindowShouldClose(window))
    {
        Renderer::SetClearColor({0.2f, 0.2f, 0.8f, 0.6f});
        Renderer::Clear();
        glfwGetWindowSize(window, &width, &height);
        if (width != prevWidth || height != prevHeight)
        {
            glViewport(0, 0, width, height);
            prevWidth = width;
            prevHeight = height;
        }
        glfwPollEvents();

        if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

        framebuffer->Bind();
        glViewport(0, 0, framebuffer->GetWidth(), framebuffer->GetHeight());

        if(appManager.GetCurrentApplicationIndex() != editor.applicationIndex)
            appManager.SetCurrentApplication(editor.applicationIndex);
        
        appManager.Update();
        framebuffer->Unbind();
        editor.Update();
        glfwSwapBuffers(window);
    }

    appManager.EndApplications();
    framebuffer->Delete();
    glfwTerminate();

    return 0;
}